# Sorta!

A passive web game where there is no true objective, but if you want, you can sort the objects on your screen to make them neat and tidy and if that makes your brain release some endorphins that's great!  

The gameplay starts with a text prompt that allows the user to type whatever object or list of objects they'd like, ex: "Old Telephones, Drinking Glasses, Bicyclo Horns", submitting sends the prompt to an AI server that generates the 3D forms (and potentially materials) of these objects and begins dumping them into a container (most of the viewport) at random. Physics and gravity are enabled which make the objects bounce off eachother playfully and at this point the user is free to "sort" the objects however they choose. There is no end to the game, but videos and screenshots should be enabled to encourage sharing.  

Can be executed with React-Three-Fiber and Drei and then an AI text-to-mesh model.  


